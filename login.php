<!DOCTYPE html>

<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="style.css" rel="stylesheet">
    <link href="bootstrap/bootstrap.min.css" rel="stylesheet">
    <script src="jquery/jquery.min.js"></script>
    <script src="bootstrap/bootstrap.min.js"></script>
</head>
    
<?php
    @session_start();
    include_once 'class.api.php';
    $hasFailedLogin = true;
    $message = "";
    $api = new API();
    if(isset($_POST['username']) && isset($_POST['password'])){
        $resp = $api->getUser($_POST['username'], $_POST['password']);
        if($resp['areValidCreds'] === true){
            $hasFailedLogin = false;
        }
        else if($resp['areValidCreds'] === false){
            $hasFailedLogin = true;
            $message = 'wrong username / password';
        }
        else if($resp['areValidCreds'] === "MULTIPLE_ACCOUNT_ERROR"){
            $hasFailedLogin = true;
            $message = "Your account's weird, contact the dev"; 
        }
    }
    if($hasFailedLogin === false){
        header('Location: index.php?extra=successLogin');
    }
    else if($hasFailedLogin === true){ ?>
    <body>
    <div class="container-fluid">
        <div class="row title">
            Login
        </div>
        <div class="row" id="loginForm">
            <form action="login.php" method="post">
                <div class="col-lg-6 col-lg-offset-3">
                    <div class="labelInput">Username</div>
                    <input type="text" placeholder="DaKing" class="input" id="usernameInput" name="username">
                </div>
                <div class="col-lg-6 col-lg-offset-3">
                    <div class="labelInput">Password</div>
                    <input type="password" placeholder="VerySecret" class="input" id="passwordInput" name="password">
                </div>
                <div class="col-lg-6 col-lg-offset-3">
                    <input class="buttonNavigation" type="submit" value="Login">
                </div>
            </form>
        </div>
        <?php if(isset($_POST["username"]) || isset($_POST["password"])){
            echo '<div class="row" id="errorLoginDiv">'.$message.'</div>';   
        }?>
    </div>
</body>
</html>
<?php
    }
?>