<?php 
    include_once 'class.api.php';
    $api = new API();
    $ids = $api->getPostsIds();
    $ids = array_reverse($ids);
?>

<!DOCTYPE html>

<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="style.css" rel="stylesheet">
    <link href="bootstrap/bootstrap.min.css" rel="stylesheet">
    <script src="jquery/jquery.min.js"></script>
    <script src="bootstrap/bootstrap.min.js"></script>
</head>

<body>
    <div class="container-fluid">
        <div class="row title">
            All dem posts
        </div>
        <div class="row" id="allPosts">
            <?php
                for($i = 0; $i < count($ids); $i++){
                    $postData = $api->getPostById($ids[$i]);
                    ?>
                    <div class="col-md-4">
                        <div class="postContainer">
                            <div class="titlePreview">
                                <?php echo $postData["title"]; ?>
                            </div>
                            <div class="contentPreview">
                                <?php
                                    if(strlen($postData["text"]) > 100){
                                        echo '<pre>'.substr($postData["text"], 0, 100).'...</pre>'; 
                                    }
                                    else{
                                        echo '<pre>'.$postData["text"].'</pre>';
                                        echo '</br>';
                                    }
                                ?>
                            </div>
                            <div class="authorPreview">
                                By <?php echo $postData["author"]; ?>
                            </div>
                            <div class="datePreview">
                                <?php echo $postData["date"]; ?>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            ?>
        </div>
    </div>
</body>

</html>