<!DOCTYPE html>

<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="style.css" rel="stylesheet">
    <link href="bootstrap/bootstrap.min.css" rel="stylesheet">
    <script src="jquery/jquery.min.js"></script>
    <script src="bootstrap/bootstrap.min.js"></script>
</head>
    
<?php 
    include_once 'class.api.php';
    session_start();
    if(isset($_POST['title'], $_POST['content'])){
        $api = new API();
        $res = $api->newPost(htmlspecialchars($_POST['title']), htmlspecialchars($_POST['content']));
    }
    else{
        $res = array(false, "NOT_SET");
    }?>
<body>
    <div class="rontainer-fluid">
        <div class="row" id="Form">
            <form action="newpost.php" method="post">
                <div class="col-lg-6 col-lg-offset-3">
                    <div class="labelInput">Title</div>
                    <input type="text" placeholder="Very Post" class="input" id="titleInput" name="title">
                </div>
                <div class="col-lg-6 col-lg-offset-3">
                    <div class="labelInput">Text</div>
                    <textarea class="textArea" id="inputTextarea" name="content" placeholder="Such Content"></textarea>
                </div>
                <div class="col-lg-6 col-lg-offset-3">
                    <input type="submit" value="Post!" class="buttonNavigation">
                </div>
            </form>
        </div>
        <?php 
            if($res[0] === true){
                ?>
                <div class="row" id="successMessage">
                    Posted!
                </div>
                <?php
            }
            else if($res[0] === false){
                if($res[1] === "NOT_LOGGED_IN"){
                    ?>
                    <div class="row" id="errorMessage">
                        Login before posting!
                    </div>
                    <?php
                }
            }
        ?>
    </div>
</body>
    
</html>