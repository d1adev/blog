<!-- 
TODO: - The Footer
      - Session System 
-->

<!DOCTYPE html>

<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="style.css" rel="stylesheet">
    <link href="bootstrap/bootstrap.min.css" rel="stylesheet">
    <script src="jquery/jquery.min.js"></script>
    <script src="bootstrap/bootstrap.min.js"></script>
</head>

<?php
    session_start();
    include_once 'class.api.php';
    $api = new API();
    $postData = $api->getLastPost();
    $arrTextToWrite = array("number" => "",
                            "date" => "",
                            "title" => "",
                            "text" => "",
                            "author" => "");
    if($postData === "QUERY_ERROR"){
        $arrTextToWrite["title"] = "Post not found";
        $arrTextToWrite["text"] = "Seems like the post you're looking for doesn't exist...";
    }
    else if($postData == "DB_ERROR"){
        $arrTextToWrite["title"] = "Querying Error";
        $arrTextToWrite["text"] = "Seems like there is an error with the Database, please try again later...";
    }
    else{
        if($postData["id"] != null) {
            $arrTextToWrite["number"] = htmlspecialchars($postData["id"]);
        }
        if($postData["title"] != null) {
            $arrTextToWrite["title"] = htmlspecialchars($postData["title"]);
        }
        if($postData["text"] != null) {
            $arrTextToWrite["text"] = htmlspecialchars($postData["text"]);
        }
        if($postData["date"] != null) {
            $arrTextToWrite["date"] = htmlspecialchars($postData["date"]);
        }
        if($postData["author"] != null) {
            $arrTextToWrite["author"] = htmlspecialchars($postData["author"]);
        }
    }
?>
    
<body>
    <div class="container-fluid">
        <div class="row" id="topBar">
            <div class="col-lg-4 buttonNavBar">
                <a href="#" class="internalLink">Home</a>
            </div>
            <div class="col-lg-4 buttonNavBar">
                <a href="#" class="internalLink">Videos</a>
            </div>
            <div class="col-lg-4 buttonNavBar">
                <a href="#" class="internalLink">Images</a>
            </div>
        </div>
        <div class="row title">
            php ftw
        </div>
        <div class="row" id="postInfos">
            <div class="col-xs-5" id="postNumber">
                <?php 
                    echo $api->postIdToString($arrTextToWrite["number"]);
                ?>
            </div>
            <div class="col-xs-2" id="author">
                By <?php echo $arrTextToWrite["author"]; ?>
            </div>
            <div class="col-xs-5" id="postDate">
                <?php 
                    echo $arrTextToWrite["date"];
                ?>
            </div>
        </div>
        <div class="row" id="actualPost">
            <div class="row" id="actualPostName">
                <?php 
                    echo $arrTextToWrite["title"];
                ?>
            </div>
            <div class="row" id="text">
                <?php 
                    echo $arrTextToWrite["text"];
                ?>
            </div>
        </div>
        <div class="row" id="postNavigation">
            <a href="posts.php">
                <div class="col-sm-offset-4 col-sm-4 buttonTripleDot">
                    See other posts
                </div>
            </a>
            
        </div>
    </div>
    <?php 
        if(isset($_GET['extra']) && $_GET['extra'] === 'successLogin'){
            $resp = $api->checkUser(session_id());
            if($resp[0] === true){
                ?>
                <script>alert('yo, <?php echo $resp[1]; ?>');</script>
                <?php
            }
        }
    ?>
</body>

</html>