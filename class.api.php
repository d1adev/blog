<?php
    class API{
        
        private $db;
        
        /* 
         DB : blog
            -> posts
                -> id (auto-increment, index, int)
                -> title (text)
                -> text (text)
                -> date (text)
                -> author(text)
            -> users
                -> id (auto-increment, index, int)
                -> username (text)
                -> password (text)
                -> sid (text)
        */
        
        public function __construct(){
            $this->connectToDB();
        }
        
// -----------------------------GENERAL ------------------------------------------------
        

        
        
// ----------------------------- POST FUNCTIONS -------------------------------------------
        
        public function getPostsIds(){
            $qry = $this->db->query("SELECT id FROM posts ORDER BY id");
            $res = $qry->fetchAll();
            $return = array();
            for($i = 0; $i < count($res); $i++){
                array_push($return, $res[$i][0]);
            }
            return $return;
        }
        
        public function fetchPosts($number){
            $sql = "SELECT * FROM posts ORDER BY id DESC LIMIT ".$number;
            $query = $this->db->query($sql);
            $res = $query->fetchAll(PDO::FETCH_ASSOC);
            return $res;
            
        }
        
        // Returns the last post data
        // Return : an associative array : [id], [title], [text], [date], [author]
        public function getLastPost(){
            if($this->db != "DB_ERROR"){
                $sql = "SELECT * FROM posts ORDER BY id DESC LIMIT 1";
                $query = $this->db->query($sql);
                if($query->rowCount() === 1){
                    $res = $query->fetch();
                    return array("id" => $res['id'],
                                 "title" => $res['title'],
                                 "text" => $res['text'],
                                 "date" => $res['date'],
                                 "author" => $res["author"]);
                }
                else{
                    return "QUERY_ERROR";
                }
            }
            else{
                return "DB_ERROR";
            }
            
        }
        
        // Returns the post data chosen by id
        // Return : if the post exists : an associative array : [id], [title], [text], [date], [author]
        public function getPostById($id){
            if($id != null && $id >= 0){
                $sql = "SELECT * FROM posts WHERE id = ".$id;
                $query = $this->db->query($sql);
                if($query->execute() && $query->rowCount() > 0){
                    $res = $query->fetchAll();
                    return array("id" => $res[0]['id'],
                                 "title" => $res[0]['title'],
                                 "text" => $res[0]['text'],
                                 "date" => $res[0]['date'],
                                 "author" => $res[0]["author"]);
                }
                else{
                    return "QUERY_ERROR";
                }
            }
            else{
                return "NO_SUCH_POST_ERROR";
            }
        }
        
        // Returns the string used when showing the number of the post.
        public function postIdToString($id){
            if($id !== ""){
                if($id[strlen($id) - 1] === "1"){
                    if($id[strlen($id) - 2] !== "1"){                    
                        return $id.'st post';
                    }
                    else{
                        return $id.'th post';
                    }
                }
                else if($id[strlen($id) - 1] === "2"){
                    if(strlen($id) > 1){
                        if($id[strlen($id) - 2] !== "1"){                    
                            return $id.'nd post';
                        }
                        else{
                            return $id.'th post';
                        }   
                    }
                }
                else if($id[strlen($id) - 1] === "3"){
                    if(strlen($id) > 2){
                        if($id[strlen($id) - 2] !== "1"){                    
                            return $id.'rd post';
                        }
                        else{
                            return $id.'th post';
                        }   
                    }
                }
                else{
                     return $id.'th post';
                }   
            }
        }
        
        public function newPost($title, $text){
            $res = $this->checkUser(session_id());
            if($res[0] === true){
                $user = $res[1];   
            }
            else{
                $user = '';
            }
            if($user !== ''){
                $req = $this->db->prepare("INSERT INTO posts (id, title, text, date, author) VALUES (NULL, :title, :text, :date, :author)");
                $req->execute(array("title" => $title,
                                    "text" => $text,
                                    "author" => $user,
                                    "date" => date("d M y")));  
                return array(true, $res[1]);
            }
            else{
                return array(false, "NOT_LOGGED_IN");
            }
        }
// ---------------------------------------DB FUNCTIONS -------------------------------------
    
        // Simple function which returns nothing, and sets $this->db to a db connection is everything went fine
        // Or to DB_ERROR if something's wrong
        private function connectToDB(){
            $db_info = array(
                "host" => "127.0.0.1",
                "port" => "3306",
                "user" => "root",
                "pass" => "",
                "name" => "blog",
                "charset" => "UTF-8"
            );
            
            try{
                $this->db = new PDO("mysql:host=".$db_info['host'].';port='.$db_info['port'].';dbname='.$db_info['name'], $db_info['user'], $db_info['pass']);
            }
            catch(PDOException $e){
                echo $e->getMessage();
                $this->db = "DB_ERROR";
            }
        }

// --------------------- USERS LOGIN FUNCTIONS --------------------------------------------
        
        
        // Login function, given a username and a password, the function will return: 
        // If the account exists, and is unique : an array : [0] : true, [1] : the sid;
        // If the account doesn't exists : an array : [0] : false
        // If the account exists but isn't unique : an array : [0] : MULTIPLE_ACCOUNT_ERROR
        // Note : [0] can be changed to ['areValidCreds'] (any case)
        //        [1] can be changed to ['sessionID'] (1st case only);
        public function getUser($username, $password){
            $req = $this->db->prepare("SELECT id FROM users WHERE username = :usrname AND password = :pwd");
            $req->execute(array('usrname' => $username,
                              'pwd' => sha1($password)));
            if($req->rowCount() === 1){
                return array('areValidCreds' => true, 'sessionID' => $this->updateSessionID($req->fetch()['id']));
            }
            else if($req->rowCount() > 1){
                // ADD LOGGING STUFF HERE
                return array('areValidCreds' => 'MULTIPLE_ACCOUNT_ERROR');
            }
            else{
                return array('areValidCreds' => false);
            }
        }
        
        // Updates the sid of an account, selected by its id
        private function updateSessionID($id){   
            $req = $this->db->prepare("UPDATE users SET sid = :usid WHERE id = :uid");
            $req->execute(array('usid' => session_id(),
                                'uid' => $id));
            return session_id();
        }
        
        
        //This function gets the username of an account given its sid. 
        public function checkUser($sid){
            $req = $this->db->prepare("SELECT username FROM users WHERE sid = :sid");
            $req->execute(array("sid" => $sid));
            if($req->rowCount() === 1){
                return array(true, $req->fetch()['username']);
            }
        }
        
        
        // This function can help against multi accounting on same sid
        // Useless for the moment, may be usefull later for sid checks
        private function deleteAccountsSidsBySid($sid){
            $req = $this->db->prepare("UPDATE users SET sid = '' WHERE sid = :usid");
            $req->execute(array("usid" => session_id()));
            if($req->execute()){
                return array(true, $req->rowCount());
            }
            else{
                return array(false, "UNKNOWN_ERROR");
            }
        }
        
// ------------------------------ USER REGISTERING FUNCTIONS -----------------------------------------
        
        // Registers the user, and sets the sid
        // Return : if the user was created : an array : [0] = true, [1] = USER_CREATED
        //          if the user wasn't created : an array : [0] = false, [1] = USER_ALREADY_EXISTS
        public function registerUser($username = null, $password = null){
            if($username !== null && $password !== null){
                if($this->checkUserAlreadyExists($username) === false){
                    $req = $this->db->prepare("INSERT INTO users (id, username, password, sid) VALUES(NULL, :username, :password, :sid)");
                    $req->execute(array("username" => $username,
                                        "password" => sha1($password),
                                        "sid" => session_id()));
                    return array(true, "USER_CREATED");
                }
                else{
                    return array(false, "USER_ALREADY_EXISTS");
                }
            }
        }
        
        // Checks if a user already exists, checking by username
        // Return : true if the user already exists
        //          false if the user doesn't exist.
        private function checkUserAlreadyExists($username = null){
            if($username !== null){
                $req = $this->db->prepare("SELECT * FROM users WHERE username = :usrname");
                $req->execute(array("usrname" => $username));
                if($req->rowCount() > 0){
                    return true;
                }
                else{
                    return false;
                }
            }
        }
    }